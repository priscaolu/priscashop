const Order = require("./Order");

const OrderState = Object.freeze({
  WELCOMING: Symbol("welcoming"),
  ITEM: Symbol("item"),
  SIZE: Symbol("size"),
  TOPPINGS: Symbol("toppings"),
  DRINKS: Symbol("drinks"),
  PAYMENT: Symbol("payment"),
});

var sizePrice = 0;
var drinkPrice = 0;

//Creating available size list
var items = ["Pizzer", "Shawarma", "Burger", "Sursage"];

//Creating available size list
var itemsize = ["Small", "Medium", "Large"];
//setting up available size prices
var itemprice = [484, 534, 645];

var tapperName = [
  "Curry", 
  "Chili", 
  "Garlic", 
  "Pepper"];

//Creating available drinks list
var drinksName = [
  "Canada Dry",
  "Cannonball",
  "Clearly Canadian",
  "Cott",
  "Pop Shoppe",
  "President's Choice",
];

//setting up available drinks prices
var drinksPrice = [184, 234, 45, 84, 54, 48];

//Generating available item size list
var itemlist = "\n";
for (var i = 0; i < items.length; i++) {
  itemlist += `${[i]} - ${items[i]}` + "\n";
}

var tapperlist = "\n";
for (var i = 0; i < tapperName.length; i++) {
  tapperlist += `${[i]} - ${tapperName[i]}` + "\n";
}

//Generating available item size list
var sizelist = "\n";
for (var i = 0; i < itemsize.length; i++) {
  sizelist += `${[i]} - ${itemsize[i]}: $${itemprice[i]}` + "\n";
}

//Generating available drinks list
var drinklist = "\n";
for (var i = 0; i < drinksName.length; i++) {
  drinklist += `${[i]} - ${drinksName[i]}: $${drinksPrice[i]}` + "\n";
}
drinklist += "\n\n" + "You may say no.";

module.exports = class ShwarmaOrder extends Order {
  constructor(sNumber, sUrl) {
    super(sNumber, sUrl);
    this.stateCur = OrderState.WELCOMING;
    this.sItem = "";
    this.sSize = "";
    this.sToppings = "";
    this.sDrinks = "";
  }
  handleInput(sInput) {
    let aReturn = [];
    switch (this.stateCur) {
      case OrderState.WELCOMING:
        this.stateCur = OrderState.ITEM;
        aReturn.push("Welcome to Prisca's Sarnie.");
        aReturn.push(`What would you like to have? ${itemlist}`);
        break;
      case OrderState.ITEM:
        //Checking if customer selection is available is the displayed list
        if (typeof items[sInput] == "undefined") {
          this.stateCur = OrderState.ITEM;
          aReturn.push(`Wrong item, kindly choose within items: ${itemlist}`);
          break;
        }
        this.stateCur = OrderState.SIZE;
        this.sItem = sInput;
        aReturn.push(
          `What size of ${items[sInput]} would you like? ${sizelist}`
        );
        break;
      case OrderState.SIZE:
        //Checking if customer selection is available is the displayed list
        if (typeof itemsize[sInput] == "undefined") {
          this.stateCur = OrderState.SIZE;
          aReturn.push(
            `Wrong size for ${
              items[this.sItem]
            }, kindly choose within size: ${sizelist}`
          );
          break;
        }
        this.stateCur = OrderState.TOPPINGS;
        this.sSize = sInput;
        aReturn.push(`What spice would you like for ${items[this.sItem]}: ${tapperlist}`);
        break;
      case OrderState.TOPPINGS:
        //Checking if customer selection is available is the displayed list
        if (typeof tapperName[sInput] == "undefined") {
          this.stateCur = OrderState.TOPPINGS;
          aReturn.push(
            `Wrong spice for ${
              items[this.sItem]
            }, kindly choose within Spice for ${items[this.sItem]}: ${tapperlist}`
          );
          break;
        }
        this.stateCur = OrderState.DRINKS;
        this.sToppings = sInput;
        aReturn.push(`Would you like drinks with that? ${drinklist}`);
        break;
      case OrderState.DRINKS:
        if (sInput.toLowerCase() != "no") {
          //Checking if customer selection is available is the displayed list
          if (typeof drinksName[sInput] == "undefined") {
            this.stateCur = OrderState.DRINKS;
            aReturn.push(
              `Wrong drink, kindly choose within available drinks: ${drinklist}`
            );
            break;
          }
          this.sDrinks = sInput;
        }
        this.stateCur = OrderState.PAYMENT;
        aReturn.push("Thank-you for your order of");

        //Setting any prevoius drinks select value to 0
        drinkPrice = 0;
        //Checking if drinks is choosen
        if (this.sDrinks) {
          drinkPrice = drinksPrice[`${this.sDrinks}`];
        }
        var orderInfo = "";
        sizePrice = itemprice[`${this.sSize}`];
        var total = sizePrice + drinkPrice;
        switch (drinkPrice) {
          case 0:
            //Customer order reciept pushing
            orderInfo =
              "Order Detail:" +
              "\n" +
              itemsize[`${this.sSize}`] +
              " " +
              items[`${this.sItem}`] +
              ` with ${tapperName[this.sToppings]} Spice:  $` +
              itemprice[`${this.sSize}`] +
              "\n" +
              `Total: $${total}`;

            aReturn.push(orderInfo);
            break;
          default:
            orderInfo =
              "Order Detail:" +
              "\n" +
              itemsize[`${this.sSize}`] +
              " " +
              items[`${this.sItem}`] +
              ` with ${tapperName[this.sToppings]} Spice:  $` +
              itemprice[`${this.sSize}`] +
              "\n" +
              "Drinks " +
              drinksName[`${this.sDrinks}`] +
              ": $" +
              drinksPrice[`${this.sDrinks}`] +
              "\n" +
              `Total: $${total}`;
            aReturn.push(orderInfo);
            break;
        }
        this.nOrder = total;
        aReturn.push(`Please pay for your order here`);
        aReturn.push(`${this.sUrl}/payment/${this.sNumber}/`);
        break;
      case OrderState.PAYMENT:
        console.log(sInput);
        this.isDone(true);
        let d = new Date();
        d.setMinutes(d.getMinutes() + 20);
        aReturn.push(`Your order will be delivered at ${d.toTimeString()}`);
        break;
    }
    return aReturn;
  }
  renderForm(sTitle = "-1", sAmount = "-1") {
    // your client id should be kept private
    if (sTitle != "-1") {
      this.sItem = sTitle;
    }
    if (sAmount != "-1") {
      this.nOrder = sAmount;
    }
    const sClientID =
      process.env.SB_CLIENT_ID ||
      "ASzVA0hME5F6nEiSkIiLRKGOI_EGj4tNPv1xZC2ko35yYSoUbEA2EnNdm7sbzm9BCHHsx-w00lBxZdia";
    return `
      <!DOCTYPE html>
  
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->
      </head>
      
      <body>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script
          src="https://www.paypal.com/sdk/js?client-id=${sClientID}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
        </script>
        Thank you ${this.sNumber} for your ${this.sItem} order of $${this.nOrder}.
        <div id="paypal-button-container"></div>
  
        <script>
          paypal.Buttons({
              createOrder: function(data, actions) {
                // This function sets up the details of the transaction, including the amount and line item details.
                return actions.order.create({
                  purchase_units: [{
                    amount: {
                      value: '${this.nOrder}'
                    }
                  }]
                });
              },
              onApprove: function(data, actions) {
                // This function captures the funds from the transaction.
                return actions.order.capture().then(function(details) {
                  // This function shows a transaction success message to your buyer.
                  $.post(".", details, ()=>{
                    window.open("", "_self");
                    window.close(); 
                  });
                });
              }
          
            }).render('#paypal-button-container');
          // This function displays Smart Payment Buttons on your web page.
        </script>
      
      </body>
          
      `;
  }
};
