Author: Prisca Olumoya
##Project Name
**Prisca shopping
  Simple chatapp created using Javascript, html and css. Customers can order using the message chatbot and get an instant receipt.

Table of Contents
General Info
Technologies
Installation
Collaboration
FAQs
References

##Table of Contents
<a name="general-info"></a>

##General Info
The Project is an online message chatbot for ordering goods. Project status: completed

##Technologies
A list of technologies used within the project: javascript, nodes.js: Version 12.3

##Installation
A little intro about the installation. install it locally by starting the terminal using npm.

$ git clone https://priscaolu@bitbucket.org/priscaolu/prisca_store.git
$ cd ../path/to/the/file
$ npm install
$ npm start

##Collaboration
Provide instructions on how to collaborate with your project. I collaborated with Olatunde

##FAQs
A list of frequently asked questions 
1. How do i order? npm start and access through local host 
2. How to make changes to the food items? :go to index.js

##References
for more information see <a href="https://github.com/rhildred/ES6OrderBot" target="_blank">ES6 Order Bot</a>

The interface for the web  was from  Pat Wilken.

To run:

1. Sign up for paypal developer sandbox and get a client id
2. The first time run `npm install`
3. `SB_CLIENT_ID=<put_in_your_client_id> npm start`